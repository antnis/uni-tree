#!/usr/bin/env ruby
# encoding: utf-8

$LOAD_PATH<<'./lib'
require 'csv'
require 'pathname'
#require 'uni'

fname,target=ARGV

unis=Hash.new
unknown=[]
f = File.open(target,"w")

def hs str
  return "root" unless str
  h=str.hash.to_s
  "u"+h[1..-1]
end

#import CVS
CSV.foreach(fname) do |row|
  if row[0]==("№".force_encoding("IBM866")) then
  #if row[0]=="$$" then # костыль
	#p row
    $year=row[1]
    next
  end
  next unless row[0]

  #Похоже пора делать структуру :(

  _,name,status,parent,_,img=row
  unis[name]=[nil,nil,nil] unless unis.has_key? name
  oyear,ostatus,oparent,oimg = unis[name]

  status = ostatus if status=="???" || ostatus
  parent = oparent if parent=~/^#{"не".force_encoding("IBM866")}/ || oparent
  parent = oparent if parent=="???" || oparent
  img||=oimg
  year=oyear ? ($year+'/'+oyear) : $year

  unis[name]=[year,status,parent,img]
end

#form unknown

unis.each_value do |v|
  _,_,parent=v
 next unless parent
 #parent = parent.encode(Encoding::UTF_8) if parent
 unknown<<parent unless unis.has_key? parent
end


#dot header
f.puts "digraph uni {"

#dot declarations
f.puts "root [label=\"???\"]"

unis.each_pair do |k,v|
  var=hs(k)
  year,status,_,img=v
  label =%!<TABLE VALIGN="MIDDLE" width="400" height="350" border="0" cellborder="0">!
  label+= "<TR><TD>#{k}</TD></TR>"
  label+= "<TR><TD>набор #{year}</TD></TR>".force_encoding("IBM866") if year
  label+= "<TR><TD> #{status} </TD></TR>" if status
  label+= "</TABLE>"
  f.puts "#{var} [label=<#{label}> shape=box];"
end

unknown.each do |k|
  var=hs(k)
  f.puts "#{var} [label=\"#{k}\"];"
end

#make parents
unis.each_pair do |k,v|
  _,_,parent=v
  f.puts "#{hs(parent)} -> #{hs(k)};"
end

#group by year                   
unis = unis.group_by {|k,v| v[0]}
unis.each_pair do |year,values|
  f.puts "{ rank=same;"
  values.each do |name,_|
    f.puts "#{hs(name)};"
  end
  f.puts "}"
end

#finalizing output
f.puts "}"
f.close
